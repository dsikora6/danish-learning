/* REQUIRING PACKAGES */

const express = require("express");
const Flashcard = require("../models/flashcards");

const router = express.Router();

/* FUNCTION */

const parseFlashcards = (text) => {
  const array = [];
  text = text.split("!");
  const category = text[0].trim();
  const flashcards = text[1].trim().split("\n");
  flashcards.forEach((flashcard) => {
    const obj = {};
    obj.category = category;
    flashcard = flashcard.split(";-;");
    obj.native = flashcard[0].trim();
    obj.pronunciation = flashcard[1].trim();
    obj.foreignBasic = flashcard[2].trim();
    obj.foreignAccepted = [];
    for(let i = 2; i < flashcard.length; i++) {
      const card = flashcard[i].trim();
      if(card) {
        obj.foreignAccepted.push(card);
      }
    }
    array.push(obj);
  });
  return array;
};

/* DEFINING ROUTES */

const correctPassword = process.env.PASSWORD;

/* ROUTES - NEW FLASHCARDS */

router.get("/", (req, res) => {
  res.render("flashcard/new", {topic: "New flashcards"});
});

router.post("/", (req, res) => {
  let {password, flashcards} = req.body.new;
  if(password === correctPassword) {
    flashcards = parseFlashcards(flashcards);
    Flashcard.insertMany(flashcards, (err, cards) => {
      if(err) {
        console.log(err);
        res.redirect("/");
      }
      else {
        console.log(cards);
        res.redirect("/flashcard");
      }
    });
  }
  else {
    res.redirect("/");
  }
});

router.get("/card/:language/:card", (req, res) => {
  const {language, card} = req.params;
  Flashcard.findOne({[language]: card}, (err, flashcard) => {
    if(err || !flashcard) {
      console.log("Flashcard not found");
      res.redirect("/");
    }
    else {
      let text = `${flashcard.category}!
      ${flashcard.native} ;-; ${flashcard.pronunciation} ;-; ${flashcard.foreignAccepted.join(" ;-; ")}`;
      res.render("flashcard/edit", {topic: "Edit flashcard", flashcard: text, id: flashcard._id});
    }
  })
});

router.put("/:id", (req, res) => {
  let {password, flashcard} = req.body.edit;
  flashcard = parseFlashcards(flashcard)[0];
  if(password === correctPassword) {
    Flashcard.findByIdAndUpdate(req.params.id, flashcard, (err, card) => {
      if(err) {
        console.log(err);
        res.redirect("/flashcard")
      }
      else {
        console.log(card);
        res.redirect("/");
      }
    });
  }
  else {
    res.redirect("/");
  }
});

router.delete("/:id", (req, res) => {
  if(req.body.password === correctPassword) {
    Flashcard.findByIdAndDelete(req.params.id, (err, card) => {
      if(err) {
        console.log(err);
        res.redirect("/flashcard");
      }
    });
  }
  else {
    res.redirect("/");
  }
});

/* ROUTES - API */

router.get("/category/:category", (req, res) => {
  Flashcard.find({category: req.params.category}, (err, cards) => {
    if(err) {
      console.log(err);
      res.json({error: "Error"});
    }
    else {
      res.json(cards);
    }
  });
});

/* EXPORTING */

module.exports = router;