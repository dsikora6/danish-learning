/* REQUIRING PACKAGES */
const Category = require("../models/categories");
const express = require("express");

const router = express.Router();

/* DEFINING ROUTES */

const correctPassword = process.env.PASSWORD;

/* SHOW FORM TO CREATE NEW CATEGORY */

router.get("/", (req, res) => {
  res.render("category/new", {topic: "New category"});
});

/* PARSE FORM, CREATE NEW CATEGORY */
router.post("/", (req, res) => {
  const category = req.body.new;
  if(category.password === correctPassword) {
    Category.create(category, (err, cat) => {
      if(err) {
        console.log(err);
        res.redirect("/");
      }
      else {
        console.log(cat);
        res.redirect("/category");
      }
    })
  }
  else {
    res.redirect("/");
  }
});

/* SHOW FORM TO EDIT CATEGORY */
router.get("/edit/:name", (req, res) => {
  category.findOne({title: req.params.name}, (err, cat) => {
    if(cat) {
      res.render("category/edit", {topic: "Edit category", category: req.params.name});
    }
    else {
      res.redirect("/");
    }
  });
});

/* EDIT CATEGORY */
router.put("/edit/:name", (req, res) => {
  const category = req.body.edit;
  if(category.password === correctPassword) {
    Category.findOneAndUpdate({title: req.params.name}, category, (err, cat) => {
      if(err || !cat) {
        console.log(err);
        res.redirect("back");
      }
      else {
        console.log(cat);
      }
    });
  }
  res.redirect(`/category/edit/${category.title}`);
});

/* REMOVE CATEGORY */
router.delete("/:name", (req, res) => {
  if(req.body.password === correctPassword) {
    Category.findOneAndDelete({title: req.params.name}, (err, cat) => {
      if(err) {
        console.log(err);
      }
    });
  }
  res.redirect("/");
})

/* API ROUTES */

/* GET LIST OF ALL CATEGORIES */
router.get("/list", (req, res) => {
  Category.find({}, (err, list) => {
    if(err) {
      const object = {name: "Error", value: err};
      res.json(object);
    }
    else {
      res.json(list);
    }
  });
});

/* EXPORTING ROUTER*/

module.exports = router;