const flashcards = document.querySelector("#flashcards");

const parseFlashcards = (text) => {
  text = text.split("	").filter(el => el.trim()).join(" ;-; ");
  return text;
}

flashcards.addEventListener("change", (e) => {
  e.target.value = parseFlashcards(e.target.value);
});