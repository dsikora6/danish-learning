class Game
{
  constructor(category, gameForm, gameInput, gameLabel, gameButton, table, resultContainer, resultText)
  {
    this.flashcards = null; // full flashcard list
    this.gameForm = gameForm; // selector for form
    this.gameInput = gameInput; // selector - input for the answer
    this.gameLabel = gameLabel; // selector - a question will be placed there
    this.gameButton = gameButton // selector for button - updating text
    this.table = table; // table for results
    this.score = 0; // total score - 0 at start
    this.currentCard = null; // current question
    this.gameOver = false; // is the game over
    this.summaryAdded = false; // check, if summary is added to avoid duplicates
    this.resultContainer = resultContainer; // container - animation at the end
    this.resultText = resultText; // animation at the end - where text will be
    
    this.gameForm.addEventListener("submit", (e) => {
      e.preventDefault(); // do not reload the page
      this.answerQuestion();
    });

    this.getFlashcards(category);
  }

  async getFlashcards(category) // asynchronous, as fetch will be used
  {
    let flashcards = await fetch(`/flashcard/category/${category}`).then((res) => res.json()); // get a full list of cards
    flashcards = this.shuffle(flashcards); // shuffle to have random order each game
    this.max = flashcards.length; // set maximum number of points
    this.flashcard = flashcards; // set the flashcard deck
    this.setQuestion(); // show the question
  }

  setQuestion()
  {
    const over = this.gameOver; // check, if the game is over
    if(!over) {
      this.gameButton.textContent = `Odpowiedz (${this.max - this.remaining + 1} / ${this.max})`;
      const card = this.flashcards.pop(); // get the last card from the deck
      this.card = card; // set it as current card
      this.gameLabel.textContent = card.polish; // show the question
    }
    else if(over && !this.summaryAdded) { // if the last question was showed, but animation was not played yet
      this.gameLabel.textContent = ""; // empty the question
      this.makeSummary(); // make the summary
      this.resultContainer.classList.remove("hidden"); // show animation box
      this.showResult(); // play animation
      this.gameButton.textContent = "Koniec";
      return false;
    }
    else {
      return false;
    }
  }

  answerQuestion()
  {
    const over = this.gameOver; // check, if the game is over
    const card = this.currentCard; // get current card
    if(!over) { // if the game is not over
      const answer = this.trimAnswer(this.value.toLowerCase()); // lowercase the answer
      const question = this.gameLabel.textContent.toLowerCase(); // lowercase the question
      const basic = card.foreignBasic.toLowerCase(); // lowercase the correct answer
      const accepted = card.foreignAccepted.map(el => el.toLowerCase()); // lowercase the accepted answers
      const success = accepted.findIndex(el => el === answer) !== -1; // if the answer was correct, set success to true, otherwise to false
      const pronunciation = card.pronunciation;
      if(success) { // if the answer was correct
        this.score++; // get one ponint
      }
      this.insertAnswer(success, question, basic, accepted, answer, pronunciation); // insert to the table
      this.showCorrect(success, basic, pronunciation);
    }
    else {
      return false;
    }
  }

  insertAnswer(success, question, basic, accepted, answer, pronunciation)
  {
    if(answer !== basic && success) {
      answer = `${answer}*`
    }
    basic = `${basic} ${pronunciation}`; // set the correct answer
    const row = document.createElement("div"); // create a div
    row.classList.add("table-row", "hover-lighten", "font-large"); // adding classes - row, lighten when hover and font size
    if(success) { // if answer was correct
      row.classList.add("bg-success"); // add green background
    }
    else { // otherwise
      row.classList.add("bg-failure"); // add red background
    }
    row.innerHTML = `<div class="table-cell flex flex-center-cross">${question}</div>
    <div class="table-cell flex flex-center-cross" title="${accepted.join(", ")}">${basic}</div>
    <div class="table-cell flex flex-center-cross">${answer}</div>`; // making cells
    this.table.append(row); // appending to the table
  }

  makeSummary() // when the game is over
  {
    if(!this.summaryAdded) { // and no summary is added
      const row = document.createElement("div"); // create a div
      row.classList.add("table-row", "bg-flag", "letter-spacing-small", "font-large"); // add correct classes - row, background, spacing and font size
      row.innerHTML = `<div class="table-cell flex flex-center-cross">Prawidłowo</div>
      <div class="table-cell flex flex-center-cross">${this.score}/${this.max}</div>
      <div class="table-cell flex flex-center-cross">${this.percentage.replace(".", ",")}%</div>`; // make cells
      this.table.append(row); // append to table
      this.summaryAdded = true; // make info, that summary is added
    }
    else {
      return false;
    }
  }

  showResult() // animation
  {
    const result = parseFloat(this.percentage); // parse the result

    let value = 0; // set the current points as 0
    const interval = setInterval(() => {
      value += result / 200; // as the animation will last 2 seconds, calculate how much should be added to current displayed result each 10 milliseconds
      let hue = value * 120 / 100; // calculate the hue 0% - red 0, 100% - green 120
      /* create the text - background will be gradient, which should look circular, text as number */
      this.resultText.style.backgroundImage = `radial-gradient(circle at 50% 50%, var(--black) 55%, transparent 55%), conic-gradient(from 0.75turn, hsl(${hue}, 80%, 30%), hsl(${hue}, 80%, 30%) ${value / 2}%, var(--background) ${value / 2}%, var(--background) 50%, transparent 50%, transparent`;
      this.resultText.textContent = `${value.toFixed(2).replace(".",",")}%`;
      if(Math.round(value * 100)/100 >= result) { // if the total result was reached
        clearInterval(interval); // end the interval
      }
      
    }, 10); // interval each 10 milliseconds
    setTimeout(() => {
      this.resultContainer.classList.add("hidden");
    }, 4000); // after 4 seconds hide the box
  }

  showCorrect(success, basic, pronunciation) // showing correct answer
  {
    this.over = 0; // set game to over - pausing the game
    if(success) { // if answer was correct
      this.gameInput.classList.add("color-success"); // change color to green
    }
    else { // otherwise
      this.gameInput.classList.add("color-failure"); // change color to red
    }
    this.value = `${basic} ${pronunciation}`; // show correct answer
    setTimeout(() => { // after 400 milliseconds
      this.value = ""; // empty the answer input
      this.gameInput.classList.remove("color-success", "color-failure"); // remove the color
      this.over = this.remaining; // end the game, if none cards are left
      this.setQuestion(); // set new question
    }, 400);
  }

  shuffle(array)  // shuffling algorythm
  {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  trimAnswer(answer) // trimming
  {
    answer = answer.split(" ");
    answer = answer.filter(el => el.trim());
    answer = answer.join(" ");
    return answer;
  }

  set flashcard(array)
  {
    this.flashcards = array; // save the deck
  }

  get value()
  {
    return this.gameInput.value; // fetch the answer
  }

  set value(val)
  {
    this.gameInput.value = val; // change answer box text content
  }

  get remaining()
  {
    return this.flashcards.length; // return total remaining flashcards number
  }

  set card(card)
  {
    this.currentCard = card; // save current card
  }

  set over(val)
  {
    if(val === 0) {
      this.gameOver = true; // end the game if no cards are left
    }
    else {
      this.gameOver = false; // otherwise inform, that game is not over yet
    }
  }

  get percentage()
  {
    return (this.score * 100 / this.max).toFixed(2); // return the percentage of points
  }
};

const gameForm = document.querySelector("#gameForm"); // selector for form
const gameInput = document.querySelector("#gameInput"); // selector - input for the answer
const gameLabel = document.querySelector("#gameLabel"); // selector - a question will be placed there
const gameButton = document.querySelector("#gameButton"); // selector for button - updating text
const table = document.querySelector("#table"); // table for results
const resultContainer = document.querySelector("#result"); // container - animation at the end
const resultText = document.querySelector("#result-text"); // animation at the end - where text will be

const game = new Game(topic, gameForm, gameInput, gameLabel, gameButton, table, resultContainer, resultText);