/* REQUIRING PACKAGES */

const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const methodOverride = require("method-override");
const mongoose = require("mongoose");

/* SETTING MONGOOSE */

mongoose.connect(process.env.DATABASE, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});

/* SETTING APP */

const app = express();

app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(methodOverride('_method'));

app.use(cors());

/* SETTING ROUTES */

const basicRoutes = require("./routes/basic");
const categoryRoutes = require("./routes/categories");
const flashcardRoutes = require("./routes/flashcards");
const quizRoutes = require("./routes/quiz");

app.use("/", basicRoutes);
app.use("/category", categoryRoutes);
app.use("/flashcard", flashcardRoutes);
app.use("/quiz", quizRoutes);

/* STARTING A SERVER */

app.listen(process.env.PORT, process.env.IP, () => {
  console.log("Everything is working");
});