/* REQUIRING PACKAGES */

const express = require("express");

const router = express.Router();

/* DEFINING ROUTES */

router.get("/:category", (req, res) => {
  res.render("quiz/quiz", {topic: req.params.category});
});

/* EXPORTING */

module.exports = router;