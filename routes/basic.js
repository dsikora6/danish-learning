/* REQUIRING PACKAGES */
const express = require("express");

const router = express.Router();

/* DEFINING ROUTES */

router.get("/", (req, res) => {
  res.render("main", {topic: "Language"});
});

/* EXPORTING ROUTER */
module.exports = router;