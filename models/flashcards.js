/* REQUIRING PACKAGES */

const mongoose = require("mongoose");

const { Schema, model } = mongoose;

/* DEFINING MODEL */

const flashcardSchema = new Schema({
  category: String,
  foreignBasic: String,
  foreignAccepted: [String],
  pronunciation: String,
  native: String
});

/* EXPORTING */

module.exports = model("Flashcard", flashcardSchema);