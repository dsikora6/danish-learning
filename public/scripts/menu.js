const menuList = document.querySelector("#menu-list"); // select the menu

const makeMenu = async (list) => { // asynchronous, as fetch will be used
  let topics = await fetch("/category/list").then(res => res.json()); // fetch the list of categories
  topics = topics.map(el => el.title); // change them to only titles
  topics.forEach((topic, idx) => { // for each topic
    idx++; // change index by 1 - as it should start from 1 not 0
    if(idx < 10) { // if the index is smaller than 10
      idx = `0${idx}`; // add 0, so all indexes will have two digits
    }
    const listItem = document.createElement("li"); // create list item
    const listAnchor = document.createElement("a"); // create anchor
    listAnchor.setAttribute("href", `/${mode}/${topic}`); // set the href to game route
    listAnchor.classList.add("link"); // add link class
    listAnchor.textContent = `${idx}. ${topic}`; // set content
    listItem.append(listAnchor); // append to list item
    list.append(listItem); // append to list
  });
};

makeMenu(menuList); // call the function