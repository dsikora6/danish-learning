/* REQUIRING MONGOOSE */

const mongoose = require("mongoose");

const { Schema, model } = mongoose;

/* DEFINING SCHEMA */

const categorySchema = new Schema({
  title: String
});

/* EXPORTING */

module.exports = model("Category", categorySchema);