# Language Learning
Simple add created to help learning languages. It allows to make your own cards, group them into categories and take a quiz.

The working version of the site can be found on [Heroku](http://danish-learning.herokuapp.com). Language learned: Danish, language of the site: Polish.
***

### Table of Contents
- Technologies
- Installation and usage
- Recent and future updates
- Images
***

### Technologies
- HTML5
- CSS3
- Vanilla JS
- NodeJS
- MongoDB
***

### Installation and usage
The application can be run in any modern web browser. First, the necessary NPM packages have to be installed. In CLI, navigate to the folder containing the application and run the command

    npm install

After necessary packages were installed, you need to connect the database, and set the password. By default, they are defined by the enviromental variables _process.env.**DATABASE**_, and _process.env.**PASSWORD**_. If you want to use change it, please go to the file **app.js**, and change line 11 to
    
    mongoose.connect("YOUR DATABASE HERE", {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});

The same thing has to be done in files **routes/categories.js""** (line 9), and **routes/flashcards.js** (line 36).
    
    const correctPassword = "Your password here";

If you want to run the app on your local machine, you will also need to select the port. To do this, line 39 in file **app.js** has to be changed. In place of PORT write the desired port (for example **8080**).
    
    app.listen(PORT, () => {
      console.log("Everything is working");
    });

The site will be then available by accessing localhost:PORT/.

After connecting new database, you will encounter the empty site. To add new flashcards, first go to the _/flashcard_ route. In the first field, write the selected password. Then in second field, write your cards in the following syntax:
    Category!
    word in native language ;-; foreign pronunciation ;-; word in foreign language ;-; (optional) another accepted words

##### Example:
    Travelling!
    around ;-; [ˈʁɔnˀd] ;-; rundt
    to drive ;-; [ˈkøːʌ] ;-; køre ;-; kører
    passport ;-; [ˈpas] ;-; et pas ;-; passet ;-; pas ;-; passene

It is important to divide the words with **;-;**, otherwise the function parsing your cards will return wrong objects. If you are using Excel or Calc, you can write your words in spreadsheet and copypaste them into the field - the function will parse them automatically.

To submit the form, click the button. Flashcards will be created and you will be returned to an empty form. If you were redirected to main site - something went wrong, the error message will be shown in terminal.

Then, go to the route _/category_. You will have a form once again, and you should fill it as previous. First field is for password. In the second field, write category name (important - it has to be the same as in flashcards!). Then submit a form.

After going to main route _/_ you will see the new category. After clicking on it, you will be redirected to a quiz route _/quiz/:yourcategory_. Once again, you will see a form. On the left side, you will see a word in your native language. Write a translation in the field, and press enter (or click the button). If you were correct - for half a second your answer will appear green, otherwise you will see the correct answer in red. The answer will also be added to table. If your answer will have the asterisk next to it, you have used other accepted options - you can see them by hovering over the word in the table.

After completion of the test, you will see an animation, which will show your total score.
***

### Recent and future updates
- Recent
  - 17/08/21
    - updated the parser logic
    - small stylesheet fix
    - (main website) uploaded new version of flashcards

- Future
  - adding documentation
  - adding flashcard mode
  - adding the search option

***

### Images

- Main site
  ![Main site](https://i.imgur.com/wZaIEWW.png)

- New flashcards
  ![New flashcards](https://i.imgur.com/HetFGnP.png)

- New category
  ![New category](https://i.imgur.com/w92xQlv.png)

- Quiz
  ![Quiz](https://i.imgur.com/ZNgZZ7r.png)

- Quiz results
  ![Quiz results](https://i.imgur.com/eE2bxJR.png)